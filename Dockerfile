FROM registry.gitlab.com/security-products/gemnasium:latest

RUN adduser -D myuser
USER myuser

CMD ["whoami"]
